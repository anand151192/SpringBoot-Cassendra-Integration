package com.anand.api.cassendra.domain;

import java.io.Serializable;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table
public class Student implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @PrimaryKey
    private int id;
    private String name;
    private String dept;
    public int getId()
    {
        return id;
    }
    public void setId( int id )
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public void setName( String name )
    {
        this.name = name;
    }
    public String getDept()
    {
        return dept;
    }
    public void setDept( String dept )
    {
        this.dept = dept;
    }
    public Student()
    {
        super();
        // TODO Auto-generated constructor stub
    }
    public Student( int id, String name, String dept )
    {
        super();
        this.id = id;
        this.name = name;
        this.dept = dept;
    }
    @Override
    public String toString()
    {
        return "Student [id=" + id + ", name=" + name + ", dept=" + dept + "]";
    }
    
}
