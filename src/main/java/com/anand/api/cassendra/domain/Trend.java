package com.anand.api.cassendra.domain;

import java.io.Serializable;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table
public class Trend
    implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    @PrimaryKey
    private int               id;
    private String            name;

    public int getId()
    {
        return id;
    }

    public void setId( int id )
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public Trend()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public Trend( int id, String name )
    {
        super();
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "Trend [id=" + id + ", name=" + name + "]";
    }
}
