package com.anand.api.cassendra.domain;

import org.springframework.data.cassandra.mapping.UserDefinedType;

@UserDefinedType
public class TrendCheck
{
    private String name;

    public TrendCheck( String name )
    {
        this.name = name;
    }

    public TrendCheck()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "TrendCheck [name=" + name + "]";
    }
    
}
