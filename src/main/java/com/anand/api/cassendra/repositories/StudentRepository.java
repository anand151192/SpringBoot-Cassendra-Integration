package com.anand.api.cassendra.repositories;

import org.springframework.data.repository.CrudRepository;

import com.anand.api.cassendra.domain.Student;

public interface StudentRepository extends CrudRepository<Student,Integer>
{
}
