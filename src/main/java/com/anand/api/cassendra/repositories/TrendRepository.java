package com.anand.api.cassendra.repositories;

import org.springframework.data.repository.CrudRepository;

import com.anand.api.cassendra.domain.Trend;

public interface TrendRepository extends CrudRepository<Trend,Integer>
{
}
