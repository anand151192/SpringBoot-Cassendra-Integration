package com.anand.api.cassendra.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.anand.api.cassendra.domain.Student;
import com.anand.api.cassendra.domain.Trend;
import com.anand.api.cassendra.repositories.StudentRepository;
import com.anand.api.cassendra.repositories.TrendRepository;

@Controller
@CrossOrigin(origins = "*")
public class OrderService
{
    private StudentRepository studentRepository;
    private TrendRepository   trendRepository;

    @Autowired
    public OrderService( StudentRepository studentRepository, TrendRepository trendRepository )
    {
        this.studentRepository = studentRepository;
        this.trendRepository = trendRepository;
    }

    @GetMapping(path = "/checkHealth")
    public @ResponseBody ResponseEntity<?> checkHealth()
    {
        return new ResponseEntity<String>( "Api Working!!!12345", HttpStatus.OK );
    }

    @RequestMapping(path = "/getStudent", method =
    { RequestMethod.GET })
    public @ResponseBody List<Student> getStudent()
    {
        /* Student student1 = new Student(4,"Abhi","CSE");
        
        studentRepository.save( student );*/
        List<Student> students = (List<Student>) studentRepository.findAll();
        for ( Student student : students )
        {
            System.out.println( student );
        }
        return students;
    }

    @RequestMapping(path = "/getTrend", method =
    { RequestMethod.GET })
    public @ResponseBody List<Trend> getTrend()
    {
        Trend trend1 = new Trend( 10, "Trend1" );
        Trend trend2 = new Trend( 20, "Trend2" );
        Trend trend3 = new Trend( 30, "Trend3" );
        Trend trend4 = new Trend( 40, "Trend4" );
        List<Trend> trends = new ArrayList<>();
        trends.add( trend1 );
        trends.add( trend2 );
        trends.add( trend3 );
        trends.add( trend4 );
        trendRepository.save( trends );
        /*List<Trend> trendList = (List<Trend>) trendRepository.findAll();
        for ( Trend trend : trendList )
        {
            System.out.println( trend );
        }*/
        return trends;
    }
}
