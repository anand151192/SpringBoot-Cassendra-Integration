package com.anand.api.cassendra.utility;

import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;

public class AppUtil
{
    public static String convertDate(Date date, String pattern){
        
        String dateStr;
        try{
            dateStr = DateFormatUtils.format( date, pattern );
        }catch(Exception ex){
            dateStr = "";
        }
        return dateStr;
    }
}
