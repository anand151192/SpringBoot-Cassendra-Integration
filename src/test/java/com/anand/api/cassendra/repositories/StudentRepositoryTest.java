package com.anand.api.cassendra.repositories;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.anand.api.cassendra.domain.Student;
import com.anand.api.cassendra.repositories.StudentRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class StudentRepositoryTest
{
    @Autowired
    private StudentRepository studentRepository;

    @Before
    public void setUp()
        throws Exception
    {
    }

    @Test
    public void testPersistence()
    {
        //given
        Student student = new Student( 101, "Anand", "CSE" );
        //when
        studentRepository.save( student );
        //then
        Assert.assertNotNull( student.getId() );
        Student newStudent = studentRepository.findOne( student.getId() );
        Assert.assertEquals( student.getName(), newStudent.getName() );
        Assert.assertEquals( student.getName(), newStudent.getName() );
        studentRepository.delete( student.getId() );
    }
}
