package com.anand.api.cassendra.repositories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.anand.api.cassendra.domain.Trend;
import com.anand.api.cassendra.repositories.TrendRepository;

import org.junit.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TrendRepositoryTest
{
    @Autowired
    private TrendRepository trendRepository;

    @Before
    public void setUp()
        throws Exception
    {
    }

    @Test
    public void testPersistence()
    {
        //given
        Trend trend = new Trend( 101, "Trend" );
        //when
        trendRepository.save( trend );
        //then
        Assert.assertNotNull( trend.getId() );
        Trend newTrend = trendRepository.findOne( trend.getId() );
        Assert.assertEquals( trend.getName(), newTrend.getName() );
        trendRepository.delete( trend.getId() );
    }
}
