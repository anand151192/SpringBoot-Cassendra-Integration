package com.anand.api.cassendra.utility;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AppUtilTest
{
    @Before
    public void setUp()
        throws Exception
    {
    }
    @Test
    public void testConvertDate()
    {
        try
        {
            Date date = DateUtils.parseDate( "12-04-2018", "dd-MM-yyyy" );
            String out = AppUtil.convertDate( date, "dd-MM-yyyy" );
            String out2 = AppUtil.convertDate( date, "" );
            Assert.assertEquals( out, "12-04-2018" );
            Assert.assertEquals( out2, "" );
        }
        catch ( ParseException e )
        {
            // TODO Auto-generated catch block
        }
        
    }
}
